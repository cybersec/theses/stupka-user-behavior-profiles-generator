# HumanBots actions
Collection of python scripts that represents single actions of user
## Requirements for action
### Files
- actions.py - python file with action logic
- \* - files and directories for specific action
### Variables
- NAME - name of action, must be same as name of action directory
- REQUIRED_ACTIONS - list of action names, that are required for this action
### Functions
- action(profile: Profile, fail: bool) -> Tuple[Dict, Dict]
  - main logic of action
  - arguments:
    - profile - user profile
    - fail - if action should fail
  - return value:
    - dictionary with data for communicator
    - dictionary with data for after_run function
- after_run(data: Dict) -> str
  - validate action run
  - create action run message
  - arguments - dictionary with 2 keys:
    - communicator_data - deque with data from communicator, use popleft()
    - actions_data - deque with data from action, use popleft()
  - return value:
    - message for user about action run
- can_run(profile: Profile) -> bool
  - say if action can be executed
  - arguments:
    - profile - user profile
  return value:
    - if action can be run
## Data structure
### Data for communicator
- name - name of this action step
- module - cmd/mfs
- mod_data - dictionary with data for module
- next_type - any/result
### Data from communicator
- status - OK/FAILED
- message - string data from action
- serialized - dictionary with serialized_output