# Action - create_file
Action that emulate creation of file in remote machine truth ssh connection.
## Requirements
### Actions
- ssh_login
- ssh_logout
### Programs
## Action files
- files/ - directory containing text files that could be created in remote machine. !! Must be filled with at least one file. !! 