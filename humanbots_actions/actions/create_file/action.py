import os
from typing import Tuple, Dict

from humanbot.bots.bot import Bot
from humanbot.etc import config
from humanbot.utils.utils import random_element, normalize_string, any_list_empty

NAME = 'create_file'
REQUIRED_ACTIONS = ['ssh_login', 'ssh_logout']

_ADDRESS = os.path.join(config.ACTIONS_DIR, NAME)
_FILES_DIR = 'files'
_FILES_DIR_ADDRESS = os.path.join(_ADDRESS, _FILES_DIR)
_FILES = os.listdir(_FILES_DIR_ADDRESS)
_END_CHECK = 'this_is_end_of_command'
if any_list_empty([_FILES]):
    raise Exception('Missing required files')


def _select_session(bot: Bot) -> int:
    """
    Select opened session of bot.
    :param bot: bot instance
    :return: selected session
    """
    return random_element(list(bot.sessions.keys()))


def _select_file():
    """
    Select file address to use.
    :return: selected file address
    """
    return random_element(_FILES)


def _read_file(file: str):
    """
    Read file content.
    :param file: file address
    :return: content of file
    """
    file_path = os.path.join(_FILES_DIR_ADDRESS, file)
    with open(file_path) as f:
        return f.read()


def after_run(data: Dict) -> str:
    """
    Run after action is finished.
    :param data: data in form of dictionary, containing data from communicator and data from execute
    :return: message about action
    """
    communicator_data = data['communicator_data'].popleft()
    action_data = data['actions_data'].popleft()

    bot: Bot = action_data['bot']
    bot.action_count += 1

    if action_data['status'] != communicator_data['status']:
        return f'creation of file {action_data["file"]} ' \
               f'finished with status {communicator_data["status"]} ' \
               f'(expected: {action_data["status"]})\n' \
               f'error message: {communicator_data["message"]}'

    return f'creation of file {action_data["file"]} ' \
           f'finished with status {communicator_data["status"]} ' \
           f'(expected: {action_data["status"]})'


def can_run(bot: Bot) -> bool:
    """
    Check if action can be executed.
    :param bot: bot that will execute action
    :return: True if action can be executed
    """
    return len(bot.sessions) != 0


def action(bot: Bot, fail: bool) -> Tuple[Dict, Dict]:
    """
    Create data for action executor.
    :param bot: bot that will execute action
    :param fail: should action fail
    :return: tuple of dictionaries, first for communicator and second for after_run
    """
    session = _select_session(bot)
    file = _select_file()
    content = normalize_string(_read_file(file))

    action_data = {
        'status': 'OK',
        'file': file,
        'bot': bot
    }

    communicator_data = {
        'name': f'Bot {bot.name} create file {file} on {bot.sessions[session]}',
        'module': 'cmd',
        'mod_data': {
            'cmd': f'printf "{content}" > "{normalize_string(file)}"; echo {_END_CHECK}',
            'session': session,
            'end_check': _END_CHECK
        },
        'next_type': 'any'
    }

    return communicator_data, action_data
