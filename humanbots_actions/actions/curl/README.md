# Action - curl
Action that emulate web browsing truth curl.
## Requirements
### Actions
### Programs
- [curl](https://curl.se/)
## Action files
- good_url.txt - text file with valid url addresses in each line. !! Must be filled with at least one url address. !!
- bad_url.txt - text file with invalid url addresses in each line. !! Must be filled with at least one url address. !!