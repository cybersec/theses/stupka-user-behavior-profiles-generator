import os
from typing import Tuple, Dict

from humanbot.bots.bot import Bot
from humanbot.etc import config
from humanbot.utils.utils import random_element

NAME = 'ssh_login'
REQUIRED_ACTIONS = ['ssh_logout']

_ADDRESS = os.path.join(config.ACTIONS_DIR, NAME)


def _select_role(bot: Bot):
    """
    Select role from user.
    :param bot: instance of Bot
    :return: selected role
    """
    return random_element(bot.profile.roles)


def after_run(data: Dict) -> str:
    """
    Run after action is finished.
    :param data: data in form of dictionary, containing data from communicator and data from execute
    :return: message about action
    """
    communicator_data = data['communicator_data'].popleft()
    action_data = data['actions_data'].popleft()

    bot: Bot = action_data['bot']
    bot.action_count += 1

    if action_data['status'] != communicator_data['status']:
        return f'creation of session to {action_data["server"]} ' \
               f'finished with status {communicator_data["status"]} ' \
               f'(expected: {action_data["status"]})\n' \
               f'error message: {communicator_data["message"]}'
	
    session_id = int(communicator_data['serialized'].get('session_id', 0))
    if session_id != 0 and session_id not in bot.sessions:
    	bot.sessions[session_id] = action_data['server']

    return f'creation of session to {action_data["server"]} ' \
           f'finished with status {communicator_data["status"]} ' \
           f'(expected: {action_data["status"]})'


def can_run(bot: Bot) -> bool:
    """
    Check if action can be executed.
    :param bot: bot that will execute action
    :return: True if action can be executed
    """
    return True


def action(bot: Bot, fail: bool) -> Tuple[Dict, Dict]:
    """
    Create data for action executor.
    :param bot: bot that will execute action
    :param fail: should action fail
    :return: tuple of dictionaries, first for communicator and second for after_run
    """
    role = _select_role(bot)

    action_data = {
        'status': 'OK',
        'server': role.server,
        'bot': bot
    }

    communicator_data = {
        'name': f'Bot {bot.name} open ssh to {role.server}',
        'module': 'msf',
        'mod_data': {
            'module_mod': 'scanner/ssh/ssh_login',
            'target': role.server,
            'username': role.username,
            'password': role.password
        },
        'next_type': 'any'
    }

    return communicator_data, action_data
