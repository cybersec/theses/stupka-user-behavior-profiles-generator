import os
from typing import Tuple, Dict

from humanbot.bots.bot import Bot
from humanbot.etc import config
from humanbot.utils.utils import random_element

NAME = 'ssh_logout'
REQUIRED_ACTIONS = ['ssh_login']

_ADDRESS = os.path.join(config.ACTIONS_DIR, NAME)


def _select_session(bot: Bot) -> int:
    """
    Select opened session of bot.
    :param bot: instance of Bot
    :return: selected session
    """
    return random_element(list(bot.sessions.keys()))


def after_run(data: Dict) -> str:
    """
    Run after action is finished.
    :param data: data in form of dictionary, containing data from communicator and data from execute
    :return: message about action
    """
    communicator_data = data['communicator_data'].popleft()
    action_data = data['actions_data'].popleft()

    bot: Bot = action_data['bot']
    bot.action_count += 1

    if action_data['status'] != communicator_data['status']:
        return f'close of session to {action_data["address"]} ' \
               f'finished with status {communicator_data["status"]} ' \
               f'(expected: {action_data["status"]})\n' \
               f'error message: {communicator_data["message"]}'

    bot.sessions.pop(action_data['session_id'])

    return f'close of session to {action_data["address"]} ' \
           f'finished with status {communicator_data["status"]} ' \
           f'(expected: {action_data["status"]})'


def can_run(bot: Bot) -> bool:
    """
    Check if action can be executed.
    :param bot: bot that will execute action
    :return: True if action can be executed
    """
    return len(bot.sessions) != 0


def action(bot: Bot, fail: bool) -> Tuple[Dict, Dict]:
    """
    Create data for action executor.
    :param bot: bot that will execute action
    :param fail: should action fail
    :return: tuple of dictionaries, first for communicator and second for after_run
    """
    session = _select_session(bot)

    action_data = {
        'status': 'OK',
        'session_id': session,
        'address': bot.sessions[session],
        'bot': bot
    }

    communicator_data = {
        'name': f'Bot {bot.name} close ssh to {bot.sessions[session]}',
        'module': 'cmd',
        'mod_data': {
            'cmd': 'exit',
            'session': session
        },
        'next_type': 'any'
    }

    return communicator_data, action_data
