# Action - wget
Action that emulate downloading truth wget.

Downloaded files will be saved in application tmp file.
## Requirements
### Actions
### Programs
- [wget](https://www.gnu.org/software/wget/)
## Action files
- good_url.txt - text file with valid url addresses to files in each line. !! Must be filled with at least one url address. !!
- bad_url.txt - text file with invalid url addresses to files in each line. !! Must be filled with at least one url address. !!