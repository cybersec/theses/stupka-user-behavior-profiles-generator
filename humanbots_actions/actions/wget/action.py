import os
from typing import Tuple, Dict

from humanbot.bots.bot import Bot
from humanbot.etc import config
from humanbot.utils.utils import random_element, load_files, any_list_empty

NAME = 'wget'
REQUIRED_ACTIONS = []

_ADDRESS = os.path.join(config.ACTIONS_DIR, NAME)
_USED_FILES = ['good_url.txt', 'bad_url.txt']
[_GOOD_URL, _BAD_URL] = load_files(_ADDRESS, _USED_FILES)
if any_list_empty([_GOOD_URL, _BAD_URL]):
    raise Exception('Required files cannot be empty')


def _select_url(bot: Bot, fail: bool) -> str:
    """
    Select url based on fail status.
    :param bot: bot that will use url
    :param fail: should action fail
    :return: selected url
    """
    return random_element(_BAD_URL if fail else _GOOD_URL)


def after_run(data: Dict) -> str:
    """
    Run after action is finished.
    :param data: data in form of dictionary, containing data from communicator and data from execute
    :return: message about action
    """
    communicator_data = data['communicator_data'].popleft()
    action_data = data['actions_data'].popleft()

    bot: Bot = action_data['bot']
    bot.action_count += 1

    if action_data['status'] != communicator_data['status']:
        return f'wget to url {action_data["url"]} ' \
               f'finished with status {communicator_data["status"]} ' \
               f'(expected: {action_data["status"]})\n' \
               f'error message: {communicator_data["message"]}'

    return f'wget to url {action_data["url"]} ' \
           f'finished with status {communicator_data["status"]} ' \
           f'(expected: {action_data["status"]})'


def can_run(bot: Bot) -> bool:
    """
    Check if action can be executed.
    :param bot: bot that will execute action
    :return: True if action can be executed
    """
    return True


def action(bot: Bot, fail: bool) -> Tuple[Dict, Dict]:
    """
    Create data for action executor.
    :param bot: bot that will execute action
    :param fail: should action fail
    :return: tuple of dictionaries, first for communicator and second for after_run
    """
    url = _select_url(bot, fail)

    action_data = {
        'status': 'FAIL' if fail else 'OK',
        'url': url,
        'bot': bot
    }

    communicator_data = {
        'name': f'Bot {bot.name} wget {url}',
        'module': 'cmd',
        'mod_data': {
            'cmd': f'wget {url} 2>&1'
        },
        'next_type': 'any'
    }

    return communicator_data, action_data
