import os
from typing import Tuple, Dict

from humanbot.bots.bot import Bot
from humanbot.etc import config
from humanbot.profiles.email import EmailServer, EmailAccount
from humanbot.utils.utils import random_element, load_files, normalize_string, any_list_empty

NAME = 'send_email'
REQUIRED_ACTIONS = []

_ADDRESS = os.path.join(config.ACTIONS_DIR, NAME)
_RCFILE = 'muttrc'
_RCFILE_ADDRESS = os.path.join(_ADDRESS, _RCFILE)
_RCFILE_WORKER_ADDRESS = os.path.join(config.WORKER_ACTION_DIR, NAME, _RCFILE)


_EMAILS_DIR = 'emails'
_EMAILS_DIR_ADDRESS = os.path.join(_ADDRESS, _EMAILS_DIR)
_EMAILS_WORKER_DIR_ADDRESS = os.path.join(config.WORKER_ACTION_DIR, NAME, _EMAILS_DIR)
_EMAILS = os.listdir(_EMAILS_DIR_ADDRESS)

_USED_FILES = ['addresses.txt']
[_EMAIL_ADDRESSES] = load_files(_ADDRESS, _USED_FILES)
if not os.path.exists(_RCFILE_ADDRESS) or any_list_empty([_EMAIL_ADDRESSES, _EMAILS]):
    raise Exception('Missing required files or addresses.txt are empty')


def _select_server(bot: Bot) -> EmailServer:
    """
    Select server of email provider.
    :param bot: instance of Bot
    :return: selected email provider server
    """
    return random_element(bot.profile.emails)


def _select_account(bot: Bot, server: EmailServer) -> EmailAccount:
    """
    Select email account in server.
    :param bot: instance of Bot
    :param server: email provider server
    :return: selected email account
    """
    return random_element(server.accounts)


def _select_content(bot: Bot) -> str:
    """
    Select content of email.
    :param bot: instance of Bot
    :return: content of email
    """
    return random_element(_EMAILS)


def _select_address(bot: Bot) -> str:
    """
    Select receiver email address.
    :param bot: instance of Bot
    :return: email address
    """
    return random_element(_EMAIL_ADDRESSES)


def after_run(data: Dict) -> str:
    """
    Run after action is finished.
    :param data: data in form of dictionary, containing data from communicator and data from execute
    :return: message about action
    """
    communicator_data = data['communicator_data'].popleft()
    action_data = data['actions_data'].popleft()

    bot: Bot = action_data['bot']
    bot.action_count += 1

    if action_data['status'] != communicator_data['status']:
        return f'send email to {action_data["email"]} ' \
               f'finished with status {communicator_data["status"]} ' \
               f'(expected: {action_data["status"]})\n' \
               f'error message: {communicator_data["message"]}'

    return f'send email to {action_data["email"]} ' \
           f'finished with status {communicator_data["status"]} ' \
           f'(expected: {action_data["status"]})'


def can_run(bot: Bot) -> bool:
    """
    Check if action can be executed.
    :param bot: bot that will execute action
    :return: True if action can be executed
    """
    return True


def action(bot: Bot, fail: bool) -> Tuple[Dict, Dict]:
    """
    Create data for action executor.
    :param bot: bot that will execute action
    :param fail: should action fail
    :return: tuple of dictionaries, first for communicator and second for after_run
    """
    server = _select_server(bot)
    account = _select_account(bot, server)
    content_file = normalize_string(_select_content(bot))
    subject = content_file.replace('_', ' ').replace('.txt', '')
    content_file = os.path.join(_EMAILS_WORKER_DIR_ADDRESS, content_file)
    to_address = _select_address(bot)

    action_data = {
        'status': 'OK',
        'email': to_address,
        'bot': bot
    }

    communicator_data = {
        'name': f'Bot {bot.name} send email to {to_address}',
        'module': 'cmd',
        'mod_data': {
            'cmd': f'export MUTT_PASS={account.password}; '
                   f'export MUTT_URL={server.create_url(account.username)}; '
                   f'export MUTT_REALNAME="{bot.profile.name}"; '
                   f'export MUTT_EMAIL={account.address}; '
                   f'mutt -F {_RCFILE_WORKER_ADDRESS} -s "{subject}" '
                   f'{to_address} < "{content_file}"; ' 
                   'unset MUTT_PASS MUTT_URL'
        },
        'next_type': 'any'
    }

    return communicator_data, action_data
