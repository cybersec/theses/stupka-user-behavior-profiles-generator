# Action - send_email
Action that emulate sending email.
## Requirements
### Actions
### Programs
- [mutt](http://www.mutt.org/)
## Action files
- muttrc - configuration file for mutt. !!Don't touch or kitten will die. !!
- addresses.txt - text file with email addresses of receivers in each line. !! Must be filled with at least one email address. !!
- emails/ - directory containing files with email contents, file name will be used as subject. !! Must be filled with at least one file. !! 