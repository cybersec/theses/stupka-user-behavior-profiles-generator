# stupka-user-behavior-profiles-generator

The content of this repository was created as a practical part of the [bachelor thesis](https://gitlab.fi.muni.cz/cybersec/theses/stupka-user-behavior-profiles-generator). The repository contains source code for emulator of user behavior based users profiles. Moreover, this repository contains set of actions that can be used with emulator. 

# Structure of repository

- **humanbot** - HumanBot component as python package
- **humanbots_actions** - actions for humanbots
- **pyproject.toml** - the definition of poetry project including dependencies
- **profiles.yaml** - example of user profiles as yaml
- **profiles.json** - example of user profiles as json
- **.env** - settings
- **.crytonenv** - settings of Cryton
- **Dockerfile** - definition of Cryton Worker image
- **docker-compose.yml** - docker compose for Cryton installation

# Requirements
 - [Poetry](https://python-poetry.org/)
 - Docker Compose
 - Docker
 - [Cryton](https://cryton.gitlab-pages.ics.muni.cz/cryton-documentation/1.x/) - will be installed by docker compose

# Installation
Clone gitlab repository.
```shell
git clone git@gitlab.fi.muni.cz:cybersec/theses/stupka-user-behavior-profiles-humanbot.git
```
```shell
cd stupka-user-behavior-profiles-humanbot
```
## Cryton
Build docker image for Cryton Worker.
```shell
docker build -t cryton_worker .
```

Set CRYTON_WORKER_NAME in .crytonenv a CRYTON_WORKER in .env to same value.

Start Cryton
```shell
docker compose --env-file .crytonenv up -d
```
## HumanBot
```shell
poetry install
```

# Settings
Change your settings in .env file. If you run this app from this repository and use docker compose to run Cryton, required setting must be chosen.

| name             | value   | default                   | Required                  | description                               |
|------------------|---------|---------------------------|---------------------------|-------------------------------------------|
| CRYTON_WORKER    | string  | humabbots_worker          | humabbots_worker          | Name of Cryton Worker.                    |
| CRYTON_API_HOST  | string  | 127.0.0.1                 | 127.0.0.1                 | Cryton Core's API host.                   |
| CRYTON_API_PORT  | int     | 8000                      | 8000                      | Cryton Core's API port.                   |
| CRYTON_API_SSL   | boolean | false                     | false                     | Use SSL to connect to Cryton Core's API.  |
| CRYTON_API_ROOT  | string  | api/                      | api/                      | Cryton Core's API root.                   |
| ACTIONS_DIR      | string  | humanbots_actions/actions | humanbots_actions/actions | Path to directory with actions.           |
| PROFILES_FILE    | string  | profiles.yaml             | -                         | Path to profiles.                         |
| MAX_RUNTIME      | int     | 60                        | -                         | Maximal runtime in minutes                |

# User profiles
Create user profiles and save them to file in yaml or json format and set PROFILES_FILE to this file. Other option is change profiles.yaml. If you decided to create your own profile, follow these [instructions](#profiles-creation-manual).
In case that you decide to use profiles.yaml or profiles.json, set email and role data to real values. In other case, actions that use these parameters fail. You can find useful to visit section [Roles](#roles) and [Emails](#emails).

## Action settings
HumanBot use actions, that are located in directory in path ACTIONS_DIR. Also are required to fill all data files for each action. For more details, visit README for each action:
 - [curl](https://gitlab.fi.muni.cz/cybersec/theses/stupka-user-behavior-profiles-generator/-/blob/main/humanbots_actions/actions/curl/README.md)
 - [wget](https://gitlab.fi.muni.cz/cybersec/theses/stupka-user-behavior-profiles-generator/-/blob/main/humanbots_actions/actions/wget/README.md)
 - [send_email](https://gitlab.fi.muni.cz/cybersec/theses/stupka-user-behavior-profiles-generator/-/blob/main/humanbots_actions/actions/send_email/README.md)
 - [ssh_login](https://gitlab.fi.muni.cz/cybersec/theses/stupka-user-behavior-profiles-generator/-/blob/main/humanbots_actions/actions/ssh_login/README.md)
 - [ssh_logout](https://gitlab.fi.muni.cz/cybersec/theses/stupka-user-behavior-profiles-generator/-/blob/main/humanbots_actions/actions/ssh_logout/README.md)
 - [create_file](https://gitlab.fi.muni.cz/cybersec/theses/stupka-user-behavior-profiles-generator/-/blob/main/humanbots_actions/actions/create_file/README.md)
# Run emulator
If you have everything set, you can run application.
```shell
poetry run humanbot
```
If is everything set properly, you should see output like this:
```
Communication with Cryton successfully initialized
All modules successfully loaded
Loaded profiles:
[1] Frantisek Stupka
Select number of bots with profile 1:
```
If you see another output, please visit [this section](#common-errors).
Please select number of bots for each profile. After this, you will be informed about total count of created bots and process of emulation begin. During this process, no more user interaction in required.
Application will print information about each action in this format:
```
Bot 1-2 curl https://is.muni.cz

Start new plan


Plan finished

Action curl finished with message: curl to url https://is.muni.cz finished with status OK (expected: OK)
```

Application will automatically finish after runtime passed MAX_RUNTIME, which is set in minutes. Other way is to close application using CTRL+C. Application is then closed after next action is finished or user use CTRL+C again.
After that, number of finished actions for each bot are displayed and application end.
```
^CKeyboard interrupt.
Closing app...
Bot 1-0 done 0 actions.
Bot 1-1 done 0 actions.
```

# Support
Application HumanBot is tested and targeted primarily on Linux Distributions. Please keep in mind that support of other OS is not ensured.

# Common errors
## Cryton not started or set properly
```
Unable to connect to http://127.0.0.1:8000/api/templates/.
Cannot connect to Cryton.
Closing app...
```

## Action missing required files
```
Communication with Cryton successfully initialized
Cannot import action send_email: Missing required files or addresses.txt are empty
Closing app...
```

## Path to profiles not set properly
```
Communication with Cryton successfully initialized
All modules successfully loaded
Error: [Errno 2] No such file or directory: '/unknown/path'
Closing app...
```

## Profiles doesn't meet requirements
```
Communication with Cryton successfully initialized
All modules successfully loaded
0/roles/0/password: 12345678 is not of type 'string'
Profiles not valid.
Closing app...
```


# Profiles creation manual
This is manual for user profile creation. 
## Profile
| name           | value  | limits                    | example   | description           |
|----------------|--------|---------------------------|-----------|-----------------------|
| name           | string | -                         | Joe Doe   | User's name           |
| age            | int    | 0 < age                   | 25        | User's age            |
| sex            | string | male/female               | male      | User's sex            |
| emails         | list   | -                         | see below | User's emails         |
| birth_location | string | -                         | Praha     | User's birth location |
| residence      | string | -                         | Brno      | User's residence      |
| pc_proficiency | int    | 0 <= pc_proficiency <= 10 | 8         | User's pc proficiency |
| paranoia       | int    | 0 <= paranoia <= 10       | 0         | User's paranoia       |
| roles          | list   | -                         | see below | User's roles          |

## Emails
This email settings must use real values for email server. 

| name           | value  | limits   | example          | description           |
|----------------|--------|----------|------------------|-----------------------|
| protocol       | string | -        | smtps            | Email server protocol |
| server         | string | -        | realy.fi.muni.cz | Email server url      |
| port           | int    | 0 < port | 465              | Email server port     |
| accounts       | list   | -        | see below        | Email accounts        |

## Email accounts
Must be set to actual email account.

| name           | value  | limits | example              | description      |
|----------------|--------|--------|----------------------|------------------|
| address        | string | -      | xdusek@fi.muni.cz    | Account address  |
| username       | string | -      | xdusek               | Account username |
| password       | string | -      | my_ultimate_password | Account password |

For example, this is settings for FI MUNI email servers for me:
```yaml
- protocol: smtps
  server: relay.fi.muni.cz
  port: 465
  accounts:
    - address: xstupka1@fi.muni.cz
      username: xstupka1
      password: xxx
```
For more information, visit documentation of used program [mutt](http://www.mutt.org/).
## Roles
Must contain information of actual account on server.

| name     | value  | limits | example         | description   |
|----------|--------|--------|-----------------|---------------|
| server   | string | -      | aisa.fi.muni.cz | Role server   |
| username | string | -      | xdoe12          | Role username |
| password | string | -      | ultimatneheslo  | Role password |

## Example yaml
```yaml
- name: Joe Doe
  age: 25
  sex: male
  emails:
    - protocol: smpts
      server: relay.fi.muni.cz
      port: 465
      accounts:
        - address: xdoe@fi.muni.cz
          username: xdoe
          password: my_ultimate_password
  birth_location: Praha
  residence: Brno
  pc_proficiency: 8
  paranoia: 0
  roles:
    - server: aisa.fi.muni.cz
      username: xdoe12
      password: ultimatneheslo
    - server: stratus.fi.muni.cz
      username: xdoe12
      password: ultimatneheslo11
```

## Licence
This project is fully open-source and licenced under MIT license.


