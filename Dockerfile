FROM registry.gitlab.ics.muni.cz:443/cryton/cryton-worker:kali

RUN apt update && apt install -y mutt wget

ENTRYPOINT cryton-worker start
