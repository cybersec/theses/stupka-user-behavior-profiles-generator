from typing import Dict


class EmailServer:
    """
    Class for email server representation.
    """
    def __init__(self, email_server: Dict):
        self.protocol = email_server['protocol']
        self.server = email_server['server']
        self.port = email_server['port']
        self.accounts = [EmailAccount(account) for account in email_server['accounts']]

    def create_url(self, username: str) -> str:
        """
        Create url of email server for given username.
        :param username: account username
        :return: created url
        """
        return f'{self.protocol}://{username}@{self.server}:{self.port}'


class EmailAccount:
    """
    Class for email account representation.
    """
    def __init__(self, email_account: Dict):
        self.address = email_account['address']
        self.username = email_account['username']
        self.password = email_account['password']
