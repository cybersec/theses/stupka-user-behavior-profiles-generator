from jsonschema import validate, ValidationError
from typing import Dict

from humanbot.profiles.email import EmailServer
from humanbot.profiles.role import Role
from humanbot.profiles.schemas import PROFILES_SCHEMA
from humanbot.utils.utils import errprint


class Profile:
    """
    Class for profile representation.
    """
    def __init__(self, profile_id: int, data: Dict):
        self.id = profile_id
        self.name = data['name']
        self.age = data['age']
        self.sex = data['sex']
        self.emails = [EmailServer(email) for email in data['emails']]
        self.birth_location = data['birth_location']
        self.residence = data['residence']
        self.pc_proficiency = data['pc_proficiency']
        self.paranoia = data['paranoia']
        self.roles = [Role(role) for role in data['roles']]
        self.relationships = None


def validate_profiles(instance: Dict) -> bool:
    """
    Check data for profiles meet requirements.
    :param instance: instance of profiles data
    :return: True if instance is valid
    """
    try:
        validate(instance, PROFILES_SCHEMA)
    except ValidationError as e:
        errprint(f'{"/".join([str(x) for x in e.path])}: {e.message}')
        return False
    return True
