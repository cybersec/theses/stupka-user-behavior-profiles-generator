PROFILES_SCHEMA = {
    'type': 'array',
    'items': {
        'type': 'object',
        'properties': {
            'name': {
                'type': 'string',
            },
            'age': {
                'type': 'integer',
                'minimum': 0
            },
            'sex': {
                'type': 'string',
                'enum': [
                    'male',
                    'female'
                ],
            },
            'birth_location': {
                'type': 'string'
            },
            'residence': {
                'type': 'string'
            },
            'pc_proficiency': {
                'type': 'number',
                'minimum': 0,
                'maximum': 10
            },
            'paranoia': {
                'type': 'number',
                'minimum': 0,
                'maximum': 10
            },
            'emails': {
                'type': 'array',
                'items': {
                    'type': 'object',
                    'properties': {
                        'protocol': {
                            'type': 'string'
                        },
                        'server': {
                            'type': 'string'
                        },
                        'port': {
                            'type': 'integer',
                            'minimum': 0
                        },
                        'accounts': {
                            'type': 'array',
                            'items': {
                                'type': 'object',
                                'properties': {
                                    'address': {
                                        'type': 'string',
                                        'format': 'email'
                                    },
                                    'username': {
                                        'type': 'string'
                                    },
                                    'password': {
                                        'type': 'string'
                                    }
                                },
                                'required': [
                                    'address',
                                    'username',
                                    'password'
                                ]
                            },
                            'minItems': 1
                        }
                    },
                    'required': [
                        'protocol',
                        'server',
                        'port',
                        'accounts'
                    ]
                },
                'minItems': 1
            },
            'roles': {
                'type': 'array',
                'items': {
                    'type': 'object',
                    'properties': {
                        'server': {
                            'type': 'string'
                        },
                        'username': {
                            'type': 'string'
                        },
                        'password': {
                            'type': 'string'
                        }
                    },
                    'required': [
                        'server',
                        'username',
                        'password'
                    ]
                },
                'minItems': 1
            },
            'relationships': {
                'type': 'array',
                'items': {
                    'type': 'object',
                    'properties': {},
                    'required': []
                },
                'minItems': 1
            },
        },
        'required': [
            'name',
            'age',
            'sex',
            'birth_location',
            'residence',
            'pc_proficiency',
            'paranoia',
            'emails',
            'roles'
        ],
    },
    'minItems': 1
}
