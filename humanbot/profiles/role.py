from typing import List


class Role:
    """
    Class for role representation.
    """
    def __init__(self, role):
        self.server: str = role['server']
        self.username: str = role['username']
        self.password: str = role['password']
