import os
import sys
from random import randint
from typing import List, Any


def random_element(elements: List[Any]) -> Any:
    """
    Select random element from list of elements.
    :param elements: list of elements
    :return: selected element
    """
    index = randint(0, len(elements) - 1)
    return elements[index]


def normalize_string(string: str) -> str:
    """
    Normalize string to be passed to command line in double quotes.
    :param string: string for normalization
    :return: normalized string
    """
    return string.replace('\n', '\\n').replace("'", "\'").replace('"', '\\"')


def load_files(address: str, files: List[str]) -> List[List[str]]:
    """
    Load content of files.
    :param address: address to directory with files
    :param files: list of file names
    :return: list of file contents split in lines
    """
    result = []
    for file in files:
        path = os.path.join(address, file)
        with open(path) as f:
            result.append([line.strip() for line in f.readlines() if line.strip() != ''])
    return result


def any_list_empty(lists: List[List]) -> bool:
    """
    Check if any list is empty.
    :param lists: list of lists to check
    :return: True if empty list was found
    """
    for list_to_validate in lists:
        if not list_to_validate:
            return True
    return False


def errprint(*args, **kwargs) -> None:
    """
    Print to standard error.
    :return: None
    """
    print(*args, file=sys.stderr, **kwargs)
