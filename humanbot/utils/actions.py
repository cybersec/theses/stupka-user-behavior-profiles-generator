import importlib.util
import os
from types import ModuleType
from typing import Optional, List, Dict

from humanbot.etc import config
from humanbot.utils.utils import any_list_empty, errprint

ACTION_REQUIRED_ATTR = ['NAME', 'REQUIRED_ACTIONS', 'action', 'can_run', 'after_run']

Action = ModuleType
Actions = Dict[str, Action]


def validate_actions(actions: Actions) -> bool:
    """
    Validate that actions meet requirements.
    :param actions: actions to validate
    :return: True if actions are valid
    """
    for name, action in actions.items():
        for attr in ACTION_REQUIRED_ATTR:
            if not hasattr(action, attr):
                errprint(f'Action {name} doesn\'t have attribute {attr}')
                return False
        if name != action.NAME:
            errprint(f'Action names aren\'t same: {name}(directory) and {action.NAME}(attribute NAME)')
            return False
        for required in action.REQUIRED_ACTIONS:
            if required not in actions:
                errprint(f'Action {name} required action {required}')
                return False
    return True


def load_actions() -> Optional[Actions]:
    """
    Load actions located in actions directory.
    :return: Loaded actions or None
    """
    module_name = 'action'

    actions = dict()

    for action_dir in os.listdir(config.ACTIONS_DIR):
        if action_dir in ['__pycache__']:
            continue
        module_path = os.path.join(config.ACTIONS_DIR, action_dir, module_name + '.py')
        try:
            spec = importlib.util.spec_from_file_location('action', module_path)
            module = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(module)
        except Exception as ex:
            errprint(f'Cannot import action {action_dir}: {ex}')
            return None
        actions[action_dir] = module
    if any_list_empty([list(actions.keys())]):
        errprint('Action directory is empty')
        return None
    if not validate_actions(actions):
        return None
    print('All modules successfully loaded')
    return actions
