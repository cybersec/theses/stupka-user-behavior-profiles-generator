import os
from typing import List

import yaml

from humanbot.bots.bot import create_bots, Bot
from humanbot.etc import config
from humanbot.communicator.cryton.cryton_communicator import CrytonCommunicator
from humanbot.big_brain.brain import Brain
from humanbot.communicator.communicator import Communicator
from humanbot.profiles.profile import Profile, validate_profiles
from humanbot.utils.actions import load_actions
from humanbot.utils.utils import errprint


def end(bots: List[Bot], communicator: Communicator) -> None:
    """
    End application.
    :param bots: created bots
    :param communicator: created communicator
    :return: None
    """
    print('Closing app...')
    for bot in bots:
        print(bot.summarize())
    communicator.terminate()


def main() -> None:
    """
    Main function, emulate users behavior.
    :return: None
    """
    bots = []
    communicator = CrytonCommunicator()

    try:
        if not communicator.initialize():
            errprint('Cannot connect to Cryton.')
            end(bots, communicator)
            return None

        print('Communication with Cryton successfully initialized')

        actions = load_actions()
        if actions is None:
            end(bots, communicator)
            return None

        with open(config.PROFILES_FILE) as f:
            profiles_data = yaml.safe_load(f)
        if not validate_profiles(profiles_data):
            errprint('Profiles not valid.')
            end(bots, communicator)
            return None

        profiles = []
        for profile_id, profile_data in enumerate(profiles_data, 1):
            profiles.append(Profile(profile_id, profile_data))
        print('Loaded profiles:')
        for profile in profiles:
            print(f'[{profile.id}] {profile.name}')

        while True:
            for profile in profiles:
                bots += create_bots(profile)
            if bots:
                break
            print('Selected 0 bots, please select at least one.')
        print(f'Number of selected emulation bots: {len(bots)}')
        brain = Brain(bots, actions, communicator)
        print('\nStarting emulation.')
        brain.generate()

    except KeyboardInterrupt:
        errprint('Keyboard interrupt.')
    except Exception as ex:
        errprint(f'Error: {str(ex)}')

    end(bots, communicator)


if __name__ == '__main__':
    main()
