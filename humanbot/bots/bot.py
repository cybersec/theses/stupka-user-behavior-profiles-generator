from typing import Dict, List

from humanbot.profiles.profile import Profile


class Bot:
    """
    Class for bot
    """
    def __init__(self, bot_id: int, profile: Profile):
        self.id = bot_id
        self.profile = profile
        self.sessions: Dict[int, str] = {}
        self.action_count = 0
        self.name = f'{self.profile.id}-{self.id}'

    def summarize(self) -> str:
        """
        Summarize actions of bot.
        :return: summarization message
        """
        return f'Bot {self.name} done {self.action_count} actions.'


def create_bots(profile: Profile) -> List[Bot]:
    """
    Ask user for number of bots for profile and create them.
    :param profile: profile of bots
    :return: list of created bots
    """
    bots = []
    count = input(f'Select number of bots with profile {profile.id}: ')
    while not count.isdecimal() or int(count) < 0:
        count = input('Input not negative number: ')
    for bot_id in range(int(count)):
        bots.append(Bot(bot_id, profile))
    return bots
