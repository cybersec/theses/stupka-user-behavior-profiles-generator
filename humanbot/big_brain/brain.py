import time
from typing import List
from random import randint
from collections import deque

from humanbot.bots.bot import Bot
from humanbot.etc import config
from humanbot.communicator.communicator import Communicator
from humanbot.utils.actions import Actions
from humanbot.utils.utils import random_element


def select_action(bot: Bot, actions: Actions):
    """
    Select action for bot.
    :param bot: bot instance
    :param actions: list of available actions
    :return: selected action
    """
    return random_element([x for x in actions.values() if x.can_run(bot)])


def select_next(bots: List[Bot]) -> Bot:
    """
    Select next bot.
    :param bots: list of bots
    :return: selected bot
    """
    return random_element(bots)


def will_fail(bot: Bot) -> bool:
    """
    Decide if the action will fail.
    :param bot: bot instance
    :return: True if action should fail
    """
    return False


class Brain:
    """
    Class for brain.
    """
    def __init__(self, bots: List[Bot], actions: Actions, communicator: Communicator):
        self.start_time = time.time()
        self.bots = bots
        self.actions = actions
        self.communicator = communicator

    def _timeout(self) -> bool:
        """
        Check if timeout happened.
        :return: True if process should end
        """
        max_runtime = config.MAX_RUNTIME * 60
        runtime = time.time() - self.start_time

        return max_runtime > runtime

    def generate(self) -> None:
        """
        Generate users behavior.
        :return: None
        """
        while self._timeout():
            steps = []
            actions_data = deque()
            actions = deque()

            for _ in range(1):
                bot = select_next(self.bots)
                action = select_action(bot, self.actions)
                fail = will_fail(bot)

                actions.append(action)

                step_data, action_data = action.action(bot, fail)

                steps.append(step_data)
                actions_data.append(action_data)

                print(step_data['name'])

            print('\nStart new plan\n')
            communicator_data = self.communicator.execute(steps)
            if communicator_data is None:
                return
            print('\nPlan finished\n')
            for action in actions:
                action_message = action.after_run(
                    {
                        'actions_data': actions_data,
                        'communicator_data': communicator_data
                    }
                )
                print(f'Action {action.NAME} finished with message: {action_message}')

            if self.communicator.was_interrupted():
                return

            time_to_sleep = randint(0, 600) / 10 / len(self.bots)
            time.sleep(time_to_sleep)
