import os
from os import getenv
from dotenv import load_dotenv
from pathlib import Path

_DIR = Path(__file__).parent.parent.parent

APP_DIR = getenv('HUMANBOT_APP_DIR', _DIR)

load_dotenv(os.path.join(APP_DIR, ".env"))

WORKER_NAME = getenv('CRYTON_WORKER', 'humanbots_worker')
API_HOST = getenv('CRYTON_API_HOST', '127.0.0.1')
API_PORT = getenv('CRYTON_API_PORT', '8000')
API_SSL = getenv('CRYTON_API_SSL', 'false').lower() == 'true'
API_ROOT = getenv('CRYTON_API_ROOT', 'api/')
ACTIONS_DIR = getenv('ACTIONS_DIR', os.path.join(APP_DIR, os.path.join('humanbots_actions', 'actions')))
MAX_RUNTIME = int(getenv('MAX_RUNTIME')) if getenv('MAX_RUNTIME', '').isdecimal() else 60
PROFILES_FILE = getenv('PROFILES_FILE', os.path.join(APP_DIR, 'profiles.yaml'))
TMP_DIR = getenv('TMP_DIR', os.path.join(APP_DIR, 'tmp'))
WORKER_ACTION_DIR = '/app/actions'

for dir_path in [APP_DIR]:
    if not os.path.exists(dir_path):
        os.mkdir(dir_path)
