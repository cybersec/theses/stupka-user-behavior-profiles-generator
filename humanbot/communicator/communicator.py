from abc import ABC, abstractmethod
from collections import deque
from typing import Dict, List, Optional


class Communicator(ABC):
    @abstractmethod
    def initialize(self) -> True:
        pass

    @abstractmethod
    def terminate(self) -> None:
        pass

    @abstractmethod
    def execute(self, steps: List[Dict]) -> Optional[deque[Dict]]:
        pass

    @abstractmethod
    def was_interrupted(self) -> bool:
        pass
