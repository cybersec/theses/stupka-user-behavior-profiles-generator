import json
import os
import signal
from collections import deque
from yaml import safe_dump
from typing import List, Dict, Optional, Union
import time
import requests

from humanbot.etc import config
from humanbot.communicator.communicator import Communicator
from humanbot.communicator.cryton import api
from humanbot.utils.utils import errprint

TEMPLATE = 'template.yaml'
TEMPLATE_FILE = os.path.join(config.APP_DIR, 'humanbot', 'communicator', 'cryton', TEMPLATE)


def transform_response(response: Union[str, requests.Response], want_response: bool = True) -> Optional[Dict]:
    """
    Transform response to dictionary.
    :param response: API response
    :param want_response: if response is wanted , defaults to True
    :return: response as Dict or None
    """
    if type(response) is str:
        errprint(response)
        return None
    if not want_response and response.ok:
        return None
    try:
        json_response = response.json()
    except json.JSONDecodeError as e:
        errprint(f'Response cannot be converted to json, error: "{e.msg}" and response code {response.status_code}')
        return None

    if not response.ok:
        errprint(json_response)
        return None

    return json_response


def get_id(response: Optional[Dict]) -> Optional[int]:
    """
    Get id from response.
    :param response: Dict response
    :return: id or None
    """
    if response is None:
        return None
    return response['id']


def report_parser(response: Optional[Dict]) -> Optional[Dict]:
    """
    Parsed report response.
    :param response: Dict response
    :return: parsed data as Dict or None
    """
    if response is None:
        return None

    parsed = {
        'state': response['detail']['state'],
        'communicator_data': deque()
    }

    steps = response['detail']['plan_executions'][-1]['stage_executions'][-1]['step_executions']
# fix cryton bug
    if steps[-1]['state'] == 'FINISHED' and parsed['state'] == 'RUNNING':
        parsed['state'] = 'FINISHED'

    for step in steps:
        data = {
            'status': step['result'],
            'message': step['output'],
            'serialized': step['serialized_output']
        }
        parsed['communicator_data'].append(data)

    return parsed


def create_plan(steps: List[Dict]) -> str:
    """
    Create plan for Cryton from datas about steps.
    :param steps: list of datas for steps
    :return: string with yaml style plan
    """
    plan = {
        'steps': []
    }

    for index, step_data in enumerate(steps):
        step = {
            'name': step_data['name'],
            'module': 'mod_msf' if step_data['module'] == 'msf' else 'mod_cmd',
            'init': index == 0,
            'last': index == len(steps) - 1,
            'next_type': step_data['next_type'],
            'next_steps': []
        }
        if step['next_type'] != 'result' and not step['last']:
            step['next_steps'].append(steps[index + 1]['name'])
        for key, value in step_data['mod_data'].items():
            step[key] = value
        plan['steps'].append(step)

    return safe_dump(plan)


class CrytonCommunicator(Communicator):
    """
    Class to communicate with Cryton.
    """
    def __init__(self):
        self.url = api.create_rest_api_url(
            config.API_HOST,
            config.API_PORT,
            config.API_SSL
        )
        self.template = None
        self.worker = None
        self.interrupt = False
        self.original_handler = None

    def initialize(self) -> bool:
        """
        Initialize communication with Cryton.
        :return: True if process succeeded
        """
        with open(TEMPLATE_FILE) as f:
            template = get_id(
                transform_response(
                    api.post_request(
                        api_url=self.url,
                        endpoint_url=api.TEMPLATE_CREATE,
                        files={'file': f}
                    )
                )
            )
        if template is None:
            return False
        self.template = template
        return self._make_worker()

    def terminate(self) -> None:
        """
        Terminate communication with Cryton.
        :return: None
        """
        if self.template is None:
            return
        transform_response(
            api.delete_request(
                self.url,
                api.TEMPLATE_DELETE,
                self.template
            ),
            False
        )

    def execute(self, steps: List[Dict]) -> Optional[deque[Dict]]:
        """
        Execute steps on Cryton.
        :param steps: list of steps data to execute
        :return: Deque of response data or None
        """
        plan = create_plan(steps)
        plan = get_id(
            transform_response(
                api.post_request(
                    api_url=self.url,
                    endpoint_url=api.PLAN_CREATE,
                    files={'file': plan},
                    data={'template_id': self.template}
                )
            )
        )
        if plan is None:
            return None
        run = get_id(
            transform_response(
                api.post_request(
                    api_url=self.url,
                    endpoint_url=api.RUN_CREATE,
                    custom_dict={
                        'plan_id': plan,
                        'worker_ids': [self.worker]
                    }
                )
            )
        )
        if run is None:
            return None
        if transform_response(
            api.post_request(
                api_url=self.url,
                endpoint_url=api.RUN_EXECUTE,
                object_id=run
            )
        ) is None:
            return None
        self.original_handler = signal.signal(signal.SIGINT, lambda signum, frame: self._handler())
        while True:
            time.sleep(0.5)
            report = report_parser(
                transform_response(
                    api.get_request(
                        api_url=self.url,
                        endpoint_url=api.RUN_REPORT,
                        object_id=run
                    )
                )
            )
            if report is None or report['state'] == 'FINISHED':
                break
        self.original_handler = signal.signal(signal.SIGINT, self.original_handler)
        if report is None:
            return None
        return report['communicator_data']

    def was_interrupted(self):
        """
        Check if was interrupted.
        :return: True if was interrupted
        """
        return self.interrupt

    def _make_worker(self) -> bool:
        """
        Check if worker with name exists or create new.
        :return: True if process succeeded
        """
        response = transform_response(api.get_request(
            api_url=self.url,
            endpoint_url=api.WORKER_LIST
        ))
        if response is None:
            return False
        for worker in response:
            if worker['name'] == config.WORKER_NAME:
                self.worker = worker['id']
                return True
        worker = get_id(
            transform_response(
                api.post_request(
                    api_url=self.url,
                    endpoint_url=api.WORKER_CREATE,
                    data={
                        'name': config.WORKER_NAME,
                        'description': 'worker for brain'
                    }
                )
            )
        )
        if worker is None:
            return False
        self.worker = worker
        return True

    def _handler(self):
        """
        Handler for SIGINT.
        :return: None
        """
        errprint('Interrupted, will be terminated after end of plan or after CTRL+C')
        self.interrupt = True
        signal.signal(signal.SIGINT, self.original_handler)
